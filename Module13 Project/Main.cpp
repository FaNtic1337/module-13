#include <iostream>
#include "Helpers.h"

int main()
{
	int num1, num2;
	std::cout << "Enter the first integer: ";
	std::cin >> num1;
	std::cout << "\nEnter the second integer: ";
	std::cin >> num2;
	int result = SquareOfSumma(num1, num2);
	std::cout << "\nThe result of 'SquareOfSum' with arguments " << num1 << " and " << num2 << " equal to: " << result << "\n";
} 